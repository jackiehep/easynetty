package com.easynetty.demo.easynettydemo;

import com.easynetty.core.net.Client;
import com.easynetty.core.net.Server;
import com.easynetty.core.net.entity.FileInfo;
import com.easynetty.core.net.entity.FileStatus;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

@ShellComponent
public class MyCommands {

    @ShellMethod("start  a server ")
    public String start() {
        Server.startServer(2000, ServerCommandHandler::new);
        return "ok";
    }

    @ShellMethod("start a client")
    public String client() {
        Client.connect("127.0.0.1", 2000, ClientCommandHandler::new);
        return "ok";
    }

    @ShellMethod("send a file")
    public String send(String file) {
        FileInfo fileInfo = new FileInfo("file", file, FileStatus.receive);
        Client.connect("127.0.0.1", 2000, () -> new ClientFileSendHandler(fileInfo));
        return "ok";
    }
}
